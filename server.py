import random
from threading import Thread
import json
from flask import Flask, request
from flask_socketio import SocketIO
import math

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secrsadfsadet!'
socketio = SocketIO(app)

MOVEMENT_PER_CLICK = 10
HAS_STAR_MULTIPLIER = 1
HAS_NO_STAR_MUTIPLIER = 2

HAS_STAR_PROBABILITY_OF_RESSURECTION = .05
HAS_NO_STAR_PROBABILITY_OF_RESSURECTION = .01 # lower less probability

DISTANCE_FROM_STAR_FOR_CAPTURE = 15

FPS = 30
index_page_content = ""
star_position = {"x":250, "y":250}
players_state = []
pstatelen = 0


# player = { team: "red", "x": , "y": , "has_star": , sid}
def zlog(msg):
    with open("my.log", "a") as ml:
        ml.write(str(msg)+"\n")
        
def rand_pos():
    return random.randint(50,450)


def check_if_won():
    global players_state
    k = False
    tmp_ps=[]
    for p in players_state:
        if p["has_star"] and p["team"] =="black" and  math.sqrt(((0-p["x"])**2 )+((0-p["x"])**2 )) < 15:
            k= {"win": True, "team": "black"}
        if p["has_star"] and p["team"] =="red" and  math.sqrt(((500-p["x"])**2 )+((500-p["x"])**2 )) < 15:
            k= {"win": True, "team": "red"}
    if k and k["win"]:
        star_position={"x":250, "y":250}
        players_state = []
    return k

def random_ressurection():
    global players_state
    global star_position
    hs_ressurect=random.randint(0,1000) < (1000*HAS_STAR_PROBABILITY_OF_RESSURECTION)
    hns_ressurect=random.randint(0,1000) < (1000*HAS_NO_STAR_PROBABILITY_OF_RESSURECTION)
    tmp_pss=[]
    for  p in players_state:
        tmp_p = p
        if hs_ressurect and p["has_star"]:
            star_position = {"x":tmp_p["x"], "y":tmp_p["y"]}
            tmp_p["x"]=rand_pos()
            tmp_p["y"]=rand_pos()
            tmp_p["has_star"] = False
        if hns_ressurect and not p["has_star"]:
            tmp_p["x"]=rand_pos()
            tmp_p["y"]=rand_pos()
        tmp_pss.append(tmp_p)
    return {"hns": hns_ressurect, "hs":hs_ressurect}
        
            
    

def capture_fallen_star(): # capture it
    global players_state 
    distm = {} # {"cuid": distance}
    lowest_val = 10000
    cuid_get_star=None
    for p in players_state:
        distm[p["cuid"]] = math.sqrt(((star_position["x"]-p["x"])**2 )+((star_position["x"]-p["x"])**2 ))
        print p["team"]+" - "+str(distm[p["cuid"]])
        lowest_val = distm[p["cuid"]] if distm[p["cuid"]] < lowest_val else lowest_val
        
    if lowest_val > DISTANCE_FROM_STAR_FOR_CAPTURE:
        return False
    else:
        print "Star captured"
    for k in distm:
        if distm[k] == lowest_val:
            cuid_get_star = k
            
    tmp_ps = []
    for p in players_state:
        tmp_p = p
        if p["cuid"] == cuid_get_star:
            tmp_p["has_star"]=True
        tmp_ps.append(tmp_p)
    players_state = tmp_ps
    return True
        
    

def send_state():
    socketio.emit('game_state', json.dumps(players_state), broadcast=True)

def add_player_what_team():# even out num of players in team
    num_red = 0
    num_black = 0
    if len(players_state) == 0:
        return "red"
    else:
        for k in players_state:
            if k["team"]=="red":
                num_red= num_red+1
            else:
                num_black = num_black+1
    if num_black > num_red:
        return "red"
    else:
        return "black"

def add_player(cuid): # true if added, false if already exists, other wise true
    global players_state
    for k in players_state:
        if k["cuid"]==cuid:
            return False
    players_state.append({ "team": add_player_what_team(), "x":rand_pos() , "y": rand_pos(), "has_star": False, "cuid": cuid})
    socketio.emit('reply',"another player joined", broadcast=True)
    send_state()
    
    return True


def apply_direction(playerstate, direction, multiplier = 1):
        playerst= playerstate;
        if direction=="up":
            playerst["y"]= playerst["y"]-(MOVEMENT_PER_CLICK*multiplier)
        if direction=="down":
            playerst["y"]= playerst["y"]+(MOVEMENT_PER_CLICK*multiplier)
        if direction=="left":
            playerst["x"]= playerst["x"]-(MOVEMENT_PER_CLICK*multiplier)
        if direction=="right":
            playerst["x"]= playerst["x"]+(MOVEMENT_PER_CLICK*multiplier)
            
        return playerst

def update_player_state(cuid, direction):
    global players_state
    
    tmp_pstate = []
    for p in players_state:
        tmp_p = p
        if p["cuid"]==cuid:
            if p["has_star"]:
                tmp_p = apply_direction(tmp_p, direction, multiplier=HAS_STAR_MULTIPLIER)
            else:
                tmp_p = apply_direction(tmp_p, direction, multiplier=HAS_NO_STAR_MUTIPLIER)
        tmp_pstate.append(tmp_p)

    players_state = tmp_pstate


"""
def game_state_update():# more like game state stream - but fck me
    while True: # run game for 3 sec
        #print ("------------>"+ str(len(players_state)))
        send_state()
        time.sleep(1/FPS)

try:
    thread.start_new_thread(game_state_update, ())
except:
    print "game_state_update didnt start"
"""

    
@app.route('/')
def index():
    """Serve the client-side application."""
    with open("client.html") as f:
        index_page_content = f.read()
    return index_page_content;

"""
game_state -
{
    "players_state"
    "star_location"
    "hs_ressurected"
    "hns_ressurected"
    "starcaptured"
}
"""

@socketio.on('connect')
def connect():
    # not sure if agame room fucks this up
    #socketio.enter_room(sid, "agame", namespace="/game")
    socketio.emit("reply","New player connected", broadcast=True)
    socketio.emit('game_state',json.dumps({
            "players_state": players_state,
            "star_location": star_position,
            "hs_ressurected":None,
            "hns_ressurected":None,
            "starcaptured":True
        }), broadcast=True)
    print("connected")


@socketio.on('position_update')
def position_update(dat):
    cuid, direction = dat.split("|")
    update_player_state(cuid, direction)
    iswon=check_if_won()
    if iswon is not False and iswon["win"]:
        socketio.emit("game_over",iswon["team"], broadcast=True)
        socketio.emit('game_state',json.dumps({
            "players_state": players_state,
            "star_location": star_position,
            "hs_ressurected":False,
            "hns_ressurected":False,
            "starcaptured":False
        }), broadcast=True)
    else:
        #do calculations, than update all about what happened
        ressurection=random_ressurection()
        captured = capture_fallen_star()
        socketio.emit('game_state',json.dumps({
                "players_state": players_state,
                "star_location": star_position,
                "hs_ressurected":ressurection["hs"],
                "hns_ressurected":ressurection["hns"],
                "starcaptured":captured
            }), broadcast=True)

@socketio.on('initiate_game')
def initiate_game(cuid):
    #zlog("sess id: "+ request.namespace.socket.sessid)
    #socketio.enter_room(sid, "agame")
    add_player(cuid) 
    socketio.emit('reply',"Added you", broadcast=False)

@socketio.on('disconnect')
def disconnect():
    print('disconnect user')

if __name__ == '__main__':
    socketio.run(app)

